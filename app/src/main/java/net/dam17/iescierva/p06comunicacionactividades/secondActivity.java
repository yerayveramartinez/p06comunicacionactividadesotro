package net.dam17.iescierva.p06comunicacionactividades;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class secondActivity extends Activity {
    Button acept;
    Button decline;
    TextView text;


    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        Bundle extras = getIntent().getExtras();
        String s = extras.getString("usuario");
        final Intent intent = new Intent(this, MainActivity.class);

        text = (TextView) findViewById(R.id.resultTwoTextView);
        text.setText("Hola " + s + ", ¿Aceptas las condiciones?");

        acept =(Button) findViewById(R.id.acceptButton);
        acept.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                intent.putExtra("extra_text", "Aceptado");
                startActivity(intent);
                finish();
            }
        });
        decline =(Button) findViewById(R.id.declineButton);
        decline.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                intent.putExtra("extra_text", "Rechazado");
                startActivity(intent);
                finish();
            }
        });
    }

}
