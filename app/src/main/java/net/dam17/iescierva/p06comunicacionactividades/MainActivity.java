package net.dam17.iescierva.p06comunicacionactividades;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button verify;
    EditText text;
    String texto;
    TextView texticoResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        texticoResult = (TextView) findViewById(R.id.resultTextView);


        String result = getIntent().getStringExtra("extra_text");
        if(result != null) {
            texticoResult.setText("Resultado " + result);
        }



        verify =(Button) findViewById(R.id.verifyButton);
        verify.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                text = (EditText)findViewById(R.id.dataEditText);
                texto = text.getText().toString();
                lanzarActivityTwo(null, texto);
            }
        });


    }
    public void lanzarActivityTwo(View view, String text){
        Intent intent = new Intent(this, secondActivity.class);
        intent.putExtra("usuario", text);
        startActivity(intent);
    }


}
